# anseko

#### 项目介绍
云手机管理平台，基于dubbo进行远程服务调用，并通过虚拟化技术kvm, xen, vmware等，建立androidx86操作系统，对虚拟机进行统计管理，批量增加和销毁模拟器

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2018/0912/171035_6d1214b9_1437763.png "QQ截图20180912170945.png")


#### 环境要求

1. zookeeper-3.4.9
2. dubbo
3. jdk1.8
4. tomcat8
5. mysql

#### 项目结构

1. i9-slb-platform-anseko-support（dubbo配置）
2. i9-slb-platform-anseko-common（通用工具包类）
3. i9-slb-platform-anseko-api-downstream（下行服务接口定义）
4. i9-slb-platform-anseko-api-coreservice（核心服务接口定义）
5. i9-slb-platform-anseko-hypervisors（虚拟化生成模拟器接口）
6. i9-slb-platform-anseko-hypervisors-kvm（虚拟化生成模拟器kvm实现）
7. i9-slb-platform-anseko-consumer-vncserver（vncserver服务）
8. i9-slb-platform-anseko-provider-downstream（下行服务提供者）
9. i9-slb-platform-anseko-provider-coreservice（核心服务接供者）
10. i9-slb-platform-anseko-web-console（控制台）
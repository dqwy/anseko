package org.i9.slb.platform.anseko.console.utils;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.MethodConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import org.i9.slb.platform.anseko.downstream.IFileCommandExecuteRemoteService;
import org.i9.slb.platform.anseko.downstream.IMultipleCommandExecuteRemoteService;
import org.i9.slb.platform.anseko.downstream.IShellCommandExecuteRemoteService;
import org.i9.slb.platform.anseko.downstream.dto.param.FileCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 指定实例执行远程调用服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 15:33
 */
@Service("commandExecuteRemoteService")
public class CommandExecuteRemoteService {

    public void shellCommandExecute(String instance, ShellCommandParamDto shellCommandParamDto) {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("dubbo-consumer");

        ReferenceConfig<IShellCommandExecuteRemoteService> referenceConfig = new ReferenceConfig<IShellCommandExecuteRemoteService>();
        referenceConfig.setInterface(IShellCommandExecuteRemoteService.class);
        referenceConfig.setUrl("dubbo://" + instance + ":20880" + "/" + IShellCommandExecuteRemoteService.class.getName());
        referenceConfig.setApplication(applicationConfig);

        MethodConfig methodConfig = new MethodConfig();
        methodConfig.setAsync(false);
        methodConfig.setName("shellCommandExecute");

        referenceConfig.setMethods(Arrays.asList(new MethodConfig[]{methodConfig}));
        referenceConfig.get().shellCommandExecute(shellCommandParamDto);
    }

    public void shellCommandExecuteBatch(String instance, List<ShellCommandParamDto> shellCommandParamDtos) {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("dubbo-consumer");

        ReferenceConfig<IShellCommandExecuteRemoteService> referenceConfig = new ReferenceConfig<IShellCommandExecuteRemoteService>();
        referenceConfig.setInterface(IShellCommandExecuteRemoteService.class);
        referenceConfig.setUrl("dubbo://" + instance + ":20880" + "/" + IShellCommandExecuteRemoteService.class.getName());
        referenceConfig.setApplication(applicationConfig);

        MethodConfig methodConfig = new MethodConfig();
        methodConfig.setAsync(false);
        methodConfig.setName("shellCommandExecuteBatch");

        referenceConfig.setMethods(Arrays.asList(new MethodConfig[]{methodConfig}));
        referenceConfig.get().shellCommandExecuteBatch(shellCommandParamDtos);
    }

    public void fileCommandExecute(String instance, FileCommandParamDto fileCommandParamDto) {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("dubbo-consumer");

        ReferenceConfig<IFileCommandExecuteRemoteService> referenceConfig = new ReferenceConfig<IFileCommandExecuteRemoteService>();
        referenceConfig.setInterface(IFileCommandExecuteRemoteService.class);
        referenceConfig.setUrl("dubbo://" + instance + ":20880" + "/" + IFileCommandExecuteRemoteService.class.getName());
        referenceConfig.setApplication(applicationConfig);

        MethodConfig methodConfig = new MethodConfig();
        methodConfig.setAsync(false);
        methodConfig.setName("fileCommandExecute");

        referenceConfig.setMethods(Arrays.asList(new MethodConfig[]{methodConfig}));
        referenceConfig.get().fileCommandExecute(fileCommandParamDto);
    }

    public void fileCommandExecuteBatch(String instance, List<FileCommandParamDto> fileCommandParamDtos) {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("dubbo-consumer");

        ReferenceConfig<IFileCommandExecuteRemoteService> referenceConfig = new ReferenceConfig<IFileCommandExecuteRemoteService>();
        referenceConfig.setInterface(IFileCommandExecuteRemoteService.class);
        referenceConfig.setUrl("dubbo://" + instance + ":20880" + "/" + IFileCommandExecuteRemoteService.class.getName());
        referenceConfig.setApplication(applicationConfig);

        MethodConfig methodConfig = new MethodConfig();
        methodConfig.setAsync(false);
        methodConfig.setName("fileCommandExecuteBatch");

        referenceConfig.setMethods(Arrays.asList(new MethodConfig[]{methodConfig}));
        referenceConfig.get().fileCommandExecuteBatch(fileCommandParamDtos);
    }

    public void multipleCommandExecute(String instance, GroupCommandParamDto groupCommandParamDto) {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("dubbo-consumer");

        ReferenceConfig<IMultipleCommandExecuteRemoteService> referenceConfig = new ReferenceConfig<IMultipleCommandExecuteRemoteService>();
        referenceConfig.setInterface(IMultipleCommandExecuteRemoteService.class);
        referenceConfig.setUrl("dubbo://" + instance + ":20880" + "/" + IMultipleCommandExecuteRemoteService.class.getName());
        referenceConfig.setApplication(applicationConfig);

        MethodConfig methodConfig = new MethodConfig();
        methodConfig.setAsync(false);
        methodConfig.setName("multipleCommandExecute");

        referenceConfig.setMethods(Arrays.asList(new MethodConfig[]{methodConfig}));
        referenceConfig.get().multipleCommandExecute(groupCommandParamDto);
    }
}

$('#phoneList li').mouseenter(function () {
    var thisHeight = $(this).height() - 100;
    var suspensionHeight = $(this).find('.layer').height();
    var thisTop = parseInt($(window).height() - $(this).offset().top - thisHeight + $(window).scrollTop());
    var pageTop = 0;
    if (suspensionHeight > thisTop) {
        pageTop = suspensionHeight;
        $(this).find('.layer').css({'top': '-' + pageTop + 'px'});
    } else {
        $(this).find('.layer').css({'top': thisHeight + 'px'});
    }
    var suspensionWidth = $(this).find('.layer').width();
    var thisRight = parseInt($('#phoneList').width() - $(this).offset().left + $('#phoneList').offset().left + 9);
    $(this).find('.layer').show();
    var pageLeft = 0;
    if (suspensionWidth > thisRight) {
        pageLeft = thisRight - suspensionWidth;
        $(this).find('.layer').css('left', pageLeft + 'px');
    }

    $('#phoneList li').css("border", "1px solid #cccccc");
    $(this).css("border", "1px solid #095f8a");
});

$('#phoneList li').mouseleave(function () {
    $(this).find('.layer').hide();
});

$('#phoneList li').click(function () {
    var dataset = $(this).attr("data-set");
    winOpen('vncviewer/' + dataset, 'vncviewer', 800, 718);
});

function winOpen(strURL, strName, width, height) {
    var theWindow = window.open(strURL, strName, "width=" + width
        + " height=" + height
        + " scrollbars=yes "
        + " left=" + (window.screen.width - width) / 2
        + " top=" + (window.screen.height - height) / 2);
    theWindow.focus();
}

$("#create-btn").click(function () {
    $("#dialog").show();
});

//关闭灰色 jQuery 遮罩
function closeBg() {
    $("#dialog").hide();
}

$("#create-simulator-btn").click(function () {
    if (!confirm("确认要创建模拟器吗？")) {
        return;
    }
    var url = $("#form").attr("action");
    var simulatorName = $("#simulatorName").val();
    var instance = $("#instance").val();
    var data = {'simulatorName': simulatorName, 'instance': instance};
    $.post(url, data, function (result) {
        if (result.result == 0) {
            alert("创建模拟器" + simulatorName + "成功");
            $("#dialog").hide();
            window.location.reload();
        }
        else {
            alert("创建模拟器" + simulatorName + "失败");
        }
    });
});


$("#batch-start-btn").click(function () {
    if (!confirm("确认要批量启动模拟器吗？")) {
        return;
    }
    var basePath = $("#basePath").val();
    var url = basePath + "/batch_start";
    $.post(url, function (result) {
        if (result.result == 0) {
            alert("批量启动模拟器成功");
            window.location.reload();
        }
        else {
            alert("批量启动模拟器失败");
        }
    });
});

$("#batch-reboot-btn").click(function () {
    if (!confirm("确认要批量重启模拟器吗？")) {
        return;
    }
    var basePath = $("#basePath").val();
    var url = basePath + "/batch_reboot";
    $.post(url, function (result) {
        if (result.result == 0) {
            alert("批量重启模拟器成功");
            window.location.reload();
        }
        else {
            alert("批量重启模拟器失败");
        }
    });
});

$("#batch-shutdown-btn").click(function () {
    if (!confirm("确认要批量关闭模拟器吗？")) {
        return;
    }
    var basePath = $("#basePath").val();
    var url = basePath + "/batch_shutdown";
    $.post(url, function (result) {
        if (result.result == 0) {
            alert("批量关闭模拟器成功");
            window.location.reload();
        }
        else {
            alert("批量关闭模拟器失败");
        }
    });
});
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="basePath" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>云手机控制台</title>
    <meta name="description" content="云手机控制台">
    <meta name="keywords" content="云手机控制台">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp,no-store"/>
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <link rel="icon" type="image/png" href="${basePath}/static/dist/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${basePath}/static/dist/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="云手机控制台"/>
    <link rel="stylesheet" href="${basePath}/static/dist/amazeui.min.css"/>
    <link rel="stylesheet" href="${basePath}/static/docs/demo.css"/>
    <script src="${basePath}/static/dist/jquery.min.js"></script>
</head>
<body>
<input type="hidden" name="basePath" id="basePath" value="${basePath}"/>
<header class="am-topbar am-header">
    <div class="am-topbar-brand">
        <strong>云手机控制台</strong>
        <small>v1.0</small>
    </div>
    <div data-module="topbarcollapse" class="am-collapse am-topbar-collapse">
        <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right">
            <li><a href="javascript:void(0);" id="create-btn"><span class="am-icon-asterisk"></span>新建云手机</a></li>
            <li><a href="javascript:void(0);" id="batch-start-btn"><span class="am-icon-history"></span>批量启动</a></li>
            <li><a href="javascript:void(0);" id="batch-reboot-btn"><span class="am-icon-history"></span>指量重启</a></li>
            <li><a href="javascript:void(0);" id="batch-shutdown-btn"><span class="am-icon-history"></span>批量关闭</a></li>
        </ul>
    </div>
</header>

<div class="am-content">
    <!-- <div class="am-container"> -->
    <div class="am-g">
        <div class="am-u-md-12">
            <div class="am-panel am-panel-default" style="float: left; width:100%; padding-bottom: 20px;">
                <div id="expend" class="am-panel-collapse am-collapse am-in">
                    <div class="am-panel-bd">
                        <ul id="phoneList">
                            <c:forEach items="${simulatorDtos}" var="simulatorDto">
                                <li data-set="${simulatorDto.id}">
                                    <div class="phone-div">
                                        <c:if test="${simulatorDto.powerStatus == 1}">
                                            <img src="${basePath}/static/docs/phone-wifi2.png" alt="" width="115px">
                                        </c:if>
                                        <c:if test="${simulatorDto.powerStatus != 1}">
                                            <img src="${basePath}/static/docs/iphone.png" alt="" width="115px">
                                        </c:if>
                                        <div class="label">${simulatorDto.simulatorName}</div>
                                    </div>
                                    <div class="layer">
                                        <table class="am-table am-table-bordered am-table-striped">
                                            <tbody>
                                            <tr>
                                                <td>手机编号</td>
                                                <td>${simulatorDto.id}</td>
                                            </tr>
                                            <tr>
                                                <td>手机名称</td>
                                                <td>${simulatorDto.simulatorName}</td>
                                            </tr>
                                            <tr>
                                                <td>状态</td>
                                                <td>${simulatorDto.powerState.name}</td>
                                            </tr>
                                            <tr>
                                                <td>CPU核数</td>
                                                <td>${simulatorDto.cpunum} Core</td>
                                            </tr>
                                            <tr>
                                                <td>内存容量</td>
                                                <td>${simulatorDto.ramnum} MB</td>
                                            </tr>
                                            <tr>
                                                <td>VNC-SERVER</td>
                                                <td>${simulatorDto.instanceDto.remoteAddress}:${simulatorDto.vncport}</td>
                                            </tr>
                                            <tr>
                                                <td>实例位置</td>
                                                <td>${simulatorDto.instance}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dialog">
    <p class="close"><a href="#" onclick="closeBg();">关闭</a></p>
    <div>
        <form action="${basePath}/create" id="form" method="post" class="basic-grey">
            <h1>创建模拟器</span>
            </h1>
            <label style="font-size: 13px;">
                <span>模拟器名称 :</span>
                <input id="simulatorName" type="text" name="simulatorName"/>
            </label>
            <label style="font-size: 13px;">
                <span>实例名称 :</span>
                <select name="instance" id="instance">
                    <c:forEach items="${instanceDtos}" var="instanceDto">
                    <option value="${instanceDto.id}">${instanceDto.instanceName}</option>
                    </c:forEach>
                </select>
            </label>
            <label style="font-size: 13px; margin-bottom: 5px;">
                <span>&nbsp;</span>
                <input id="create-simulator-btn" type="button" class="button" value="创建模拟器"/>
            </label>
        </form>
    </div>
</div>
<script src="${basePath}/static/dist/amazeui.min.js"></script>
<script src="${basePath}/static/dist/echarts.min.js"></script>
<script src="${basePath}/static/dist/t3.min.js"></script>
<script src="${basePath}/static/docs/demo.min.js"></script>
<script src="${basePath}/static/docs/index.js"></script>
</body>
</html>

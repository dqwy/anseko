<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="basePath" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${simulatorDto.simulatorName}云手机控制台</title>
    <meta name="description" content="云手机控制台">
    <meta name="keywords" content="云手机控制台">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp,no-store"/>
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <link rel="icon" type="image/png" href="${basePath}/static/dist/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="${basePath}/static/dist/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="云手机控制台"/>
    <link rel="stylesheet" href="${basePath}/static/dist/amazeui.min.css"/>
    <link rel="stylesheet" href="${basePath}/static/docs/demo.css"/>
</head>
<body>
<input type="hidden" name="basePath" id="basePath" value="${basePath}"/>
<input type="hidden" name="simulatorId" id="simulatorId" value="${simulatorDto.id}"/>
<header class="am-topbar am-header">
    <div class="am-topbar-brand">
        <strong>【${simulatorDto.simulatorName}】云手机控制台</strong>
        <small>v1.0</small>
    </div>
    <div data-module="topbarcollapse" class="am-collapse am-topbar-collapse">
        <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right">
            <li><a class="close-btn" href="javascript:void(0);"><span class="am-icon-asterisk"></span>关闭</a></li>
        </ul>
    </div>
</header>

<div class="am-content">
    <div class="am-g">
        <div class="am-u-md-12" style="padding:0px">
            <div class="am-panel am-panel-default">
                <div id="type" class="am-panel-collapse am-collapse am-in">
                    <div class="am-panel-bd">
                        <div style="line-height: 40px; height: 40px; padding-left: 10px;">
                            <button id="start-btn">开机</button>
                            <button id="shutdown-btn">关机</button>
                            <button id="reboot-btn">重启</button>
                            <button id="destroy-btn">销毁</button>
                            <button id="focus-btn">获取用户输入焦点</button>
                            <button id="load-btn">重新加载</button>
                        </div>
                        <div id="type-main-content" render="true" style="height: 600px;">
                            <iframe id="vncview" frameborder="0" width="100%" height="100%" marginheight="0"
                                    marginwidth="0" scrolling="no" src=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${basePath}/static/dist/jquery.min.js"></script>
<script src="${basePath}/static/dist/amazeui.min.js"></script>
<script src="${basePath}/static/dist/echarts.min.js"></script>
<script src="${basePath}/static/dist/t3.min.js"></script>
<script src="${basePath}/static/docs/demo.min.js"></script>
<script src="${basePath}/static/docs/vncviewer.js"></script>
</body>
</html>

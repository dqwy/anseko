package org.i9.slb.platform.anseko.vncserver.config;

import java.io.InputStream;
import java.util.Properties;

/**
 * Guacamole配置数据
 *
 * @author R12
 * @date 2018.08.31
 */
public class GuacamoleRemoteServiceProperties {

    public String HOSTNAME;

    public int PORT;

    private static final GuacamoleRemoteServiceProperties INSTANCE = new GuacamoleRemoteServiceProperties();

    public static GuacamoleRemoteServiceProperties getInstance() {
        return INSTANCE;
    }

    public void loadProperties() {
        try {
            Properties properties = new Properties();
            InputStream inputStream = GuacamoleRemoteServiceProperties.class.getClassLoader().getResourceAsStream("config/guacd.properties");
            properties.load(inputStream);
            HOSTNAME = properties.getProperty("hostname");
            String str = properties.getProperty("port");
            PORT = Integer.parseInt(str);
            inputStream.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}

package org.i9.slb.platform.anseko.vncserver.servlet;

import org.glyptodon.guacamole.GuacamoleException;
import org.glyptodon.guacamole.net.GuacamoleSocket;
import org.glyptodon.guacamole.net.GuacamoleTunnel;
import org.glyptodon.guacamole.net.InetGuacamoleSocket;
import org.glyptodon.guacamole.net.SimpleGuacamoleTunnel;
import org.glyptodon.guacamole.protocol.ConfiguredGuacamoleSocket;
import org.glyptodon.guacamole.protocol.GuacamoleConfiguration;
import org.glyptodon.guacamole.servlet.GuacamoleHTTPTunnelServlet;
import org.glyptodon.guacamole.servlet.GuacamoleSession;
import org.i9.slb.platform.anseko.common.BusinessException;
import org.i9.slb.platform.anseko.common.ErrorCode;
import org.i9.slb.platform.anseko.provider.dto.KeyValuesDto;
import org.i9.slb.platform.anseko.vncserver.config.GuacamoleRemoteServiceProperties;
import org.i9.slb.platform.anseko.vncserver.remote.CoreserviceRemoteService;
import org.i9.slb.platform.anseko.vncserver.remote.SpringBeanService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class BasicGuacamoleTunnelServlet extends GuacamoleHTTPTunnelServlet {

    private static final long serialVersionUID = -6257946949350811664L;

    @Override
    protected GuacamoleTunnel doConnect(HttpServletRequest request) throws GuacamoleException {
        HttpSession httpSession = request.getSession(true);
        String simulatorId = (String) httpSession.getAttribute("simulatorId");
        String chk = (String) httpSession.getAttribute("chk");
        KeyValuesDto keyValuesDto = (KeyValuesDto) httpSession.getAttribute("keyValuesDto");
        if (keyValuesDto == null || "true".equals(chk)) {
            CoreserviceRemoteService coreserviceRemoteService = SpringBeanService.getBean(CoreserviceRemoteService.class);
            keyValuesDto = coreserviceRemoteService.remoteServiceSimulatorVNCKeyValues(simulatorId);
            if (keyValuesDto == null) {
                //throw new BusinessException(ErrorCode.SIMULATOR_NON_EXISTENT, "获取远程模拟器不存在");
            }
            httpSession.setAttribute("keyValuesDto", keyValuesDto);
            httpSession.setAttribute("chk", "false");
        }
        GuacamoleConfiguration guacamoleConfiguration = new GuacamoleConfiguration();
        guacamoleConfiguration.setProtocol((String) keyValuesDto.getParameter("protocol"));
        guacamoleConfiguration.setParameter("hostname", (String) keyValuesDto.getParameter("hostname"));
        guacamoleConfiguration.setParameter("port", (String) keyValuesDto.getParameter("port"));
        guacamoleConfiguration.setParameter("password", (String) keyValuesDto.getParameter("password"));

        //GuacamoleClientInformation guacamoleClientInformation = new GuacamoleClientInformation();
        //guacamoleClientInformation.setOptimalScreenWidth(480);
        //guacamoleClientInformation.setOptimalScreenHeight(800);

        String hostname = GuacamoleRemoteServiceProperties.getInstance().HOSTNAME;
        int port = GuacamoleRemoteServiceProperties.getInstance().PORT;
        GuacamoleSocket guacamoleSocket = new ConfiguredGuacamoleSocket(new InetGuacamoleSocket(hostname, port), guacamoleConfiguration);
        GuacamoleTunnel guacamoleTunnel = new SimpleGuacamoleTunnel(guacamoleSocket);
        GuacamoleSession guacamoleSession = new GuacamoleSession(httpSession);
        guacamoleSession.attachTunnel(guacamoleTunnel);
        return guacamoleTunnel;
    }
}

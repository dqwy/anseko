package org.i9.slb.platform.anseko.hypervisors;

import org.i9.slb.platform.anseko.hypervisors.param.SimulatorInfo;

/**
 * 虚拟化构建器
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:30
 */
public abstract class AbstractSimulatorBuilder {

    protected abstract String makeDefineSimulatorFile(SimulatorInfo simulatorInfo);

    public String makeDefineSimulatorFile() {
        String content = this.makeDefineSimulatorFile(this.simulatorInfo);
        return content;
    }

    protected SimulatorInfo simulatorInfo;

    public AbstractSimulatorBuilder(SimulatorInfo simulatorInfo) {
        this.simulatorInfo = simulatorInfo;
    }
}

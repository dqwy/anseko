package org.i9.slb.platform.anseko.hypervisors.param;

/**
 * 虚拟化模拟器网络描述
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:38
 */
public class SimulatorNetwork {

    /**
     * 网卡uuid
     */
    private String uuid;

    /**
     * 目标名称
     */
    private String targetName;

    /**
     * mac 地址
     */
    private String mac;

    /**
     * 限速
     */
    private Integer networkRateKBps;

    /**
     * 虚拟化模拟器网络描述构造函数
     *
     * @param uuid
     * @param targetName
     * @param mac
     * @param networkRateKBps
     */
    public SimulatorNetwork(String uuid, String targetName, String mac, Integer networkRateKBps) {
        this.uuid = uuid;
        this.targetName = targetName;
        this.mac = mac;
        this.networkRateKBps = networkRateKBps;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public Integer getNetworkRateKBps() {
        return networkRateKBps;
    }

    public void setNetworkRateKBps(Integer networkRateKBps) {
        this.networkRateKBps = networkRateKBps;
    }
}

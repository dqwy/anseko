package org.i9.slb.platform.anseko.provider.dto;

import org.i9.slb.platform.anseko.common.VirtualEnum;

/**
 * 实例DTO
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/4 10:34
 */
public class InstanceDto implements java.io.Serializable {

    private static final long serialVersionUID = 7012541683011402155L;

    public VirtualEnum getVirtualEnum() {
        VirtualEnum virtualEnum = VirtualEnum.valueOf(this.virtualType);
        return virtualEnum;
    }

    private String id;

    private String instanceName;

    private Integer virtualType;

    private String remoteAddress;

    private String createDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public Integer getVirtualType() {
        return virtualType;
    }

    public void setVirtualType(Integer virtualType) {
        this.virtualType = virtualType;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}

package org.i9.slb.platform.anseko.provider.repository.mapper;

import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.i9.slb.platform.anseko.common.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Instance数据映射
 *
 * @author R12
 * @date 2018年9月6日 16:51:02
 */
public class InstanceRowMapper implements RowMapper<InstanceDto> {

    @Override
    public InstanceDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        InstanceDto instanceDto = new InstanceDto();
        instanceDto.setId(rs.getString("id"));
        instanceDto.setInstanceName(rs.getString("instanceName"));
        instanceDto.setRemoteAddress(rs.getString("remoteAddress"));
        instanceDto.setVirtualType(rs.getInt("virtualType"));
        Date createDate = rs.getDate("createDate");
        instanceDto.setCreateDate(DateUtil.format(createDate));
        return instanceDto;
    }
}

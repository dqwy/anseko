package org.i9.slb.platform.anseko.provider.repository.mapper;

import org.i9.slb.platform.anseko.provider.dto.CommandExecuteDto;
import org.i9.slb.platform.anseko.common.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * CommandExecute数据映射
 *
 * @author R12
 * @date 2018年9月6日 16:50:37
 */
public class CommandExecuteRowMapper implements RowMapper<CommandExecuteDto> {

    @Override
    public CommandExecuteDto mapRow(ResultSet resultSet, int i) throws SQLException {
        CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
        commandExecuteDto.setCommandGroupId(resultSet.getString("commandGroupId"));
        commandExecuteDto.setCommandId(resultSet.getString("commandId"));
        commandExecuteDto.setCommandLine(resultSet.getString("commandLine"));
        Date startDate = resultSet.getDate("startDate");
        if (startDate != null) {
            commandExecuteDto.setStartDate(DateUtil.format(startDate));
        }
        Date endDate = resultSet.getDate("endDate");
        if (endDate != null) {
            commandExecuteDto.setEndDate(DateUtil.format(endDate));
        }
        commandExecuteDto.setCommandResult(resultSet.getString("commandResult"));
        commandExecuteDto.setStatus(resultSet.getInt("status"));
        return commandExecuteDto;
    }
}

package org.i9.slb.platform.anseko.downstream.dto.result;

public class CommandExecuteReDto implements java.io.Serializable {

    private static final long serialVersionUID = 7340976205824205884L;

    private String commandId;

    private String executeResult;

    public String getCommandId() {
        return commandId;
    }

    public void setCommandId(String commandId) {
        this.commandId = commandId;
    }

    public String getExecuteResult() {
        return executeResult;
    }

    public void setExecuteResult(String executeResult) {
        this.executeResult = executeResult;
    }
}

package org.i9.slb.platform.anseko.downstream;

import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;

/**
 * 混合命令远程服务调用
 *
 * @author R12
 * @date 2018.08.29
 */
public interface IMultipleCommandExecuteRemoteService {

    void multipleCommandExecute(GroupCommandParamDto groupCommandParamDto);
}
